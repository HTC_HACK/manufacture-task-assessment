package uz.pdp.task_assessment.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.task_assessment.entity.user.Role;

public interface RoleRepo extends JpaRepository<Role,String> {

    @Query(nativeQuery = true
    ,value = "select r.* from roles r where r.name<>'ROLE_SUPER_ADMIN'")
    Page<Role> getCompanyRole(Pageable pageable);
}
