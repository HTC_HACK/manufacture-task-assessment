package uz.pdp.task_assessment.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import uz.pdp.task_assessment.entity.company.Company;

@Repository
public interface CompanyRepo extends JpaRepository<Company,String> {
    Company findByName(String companyName);
}
