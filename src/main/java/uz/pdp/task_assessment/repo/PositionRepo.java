package uz.pdp.task_assessment.repo;

import org.springframework.data.jpa.repository.JpaRepository;
import uz.pdp.task_assessment.entity.company.Position;

public interface PositionRepo extends JpaRepository<Position,String> {
}
