package uz.pdp.task_assessment.repo;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import uz.pdp.task_assessment.entity.user.User;

public interface UserRepo extends JpaRepository<User, String> {

    User findByUsername(String username);

    @Query(nativeQuery = true,
    value = "select u.*\n" +
            "from users u\n" +
            "where u.company_id=:companyId")
    Page<User> findUserCompany(Pageable pageable,String companyId);
}