package uz.pdp.task_assessment.controller.rest;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.task_assessment.service.interfaces.PositionService;

import static uz.pdp.task_assessment.config.Constant.DEFAULT_PAGE;
import static uz.pdp.task_assessment.config.Constant.DEFAULT_PAGE_SIZE;


//Asatbek Xalimojnov 4/8/22 10:04 AM

@RestController
@RequestMapping("/api/position")
@AllArgsConstructor
public class PositionController {

    private final PositionService positionService;

    @GetMapping
    public ResponseEntity getAllCompanies(
            @RequestParam(required = false, defaultValue = DEFAULT_PAGE) int page,
            @RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE) int size
    ) {
        return positionService.getAllPositions(page, size);
    }

}
