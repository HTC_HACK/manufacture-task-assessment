package uz.pdp.task_assessment.controller.rest;


//Asatbek Xalimojnov 4/8/22 9:28 AM


import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task_assessment.dto.UserDto;
import uz.pdp.task_assessment.service.interfaces.UserService;

import javax.validation.Valid;

import static uz.pdp.task_assessment.config.Constant.DEFAULT_PAGE;
import static uz.pdp.task_assessment.config.Constant.DEFAULT_PAGE_SIZE;

@RestController
@RequestMapping("/api/user")
@AllArgsConstructor
public class UserController {

    private final UserService userService;

    @GetMapping
    @Secured({"ROLE_SUPER_ADMIN", "ROLE_COMPANY_ADMIN"})
    public ResponseEntity getAllUsers(
            @RequestParam(required = false, defaultValue = DEFAULT_PAGE) int page,
            @RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE) int size
    ) {
        return userService.getAllUsers(page, size);
    }

    @GetMapping("/{userId}")
    @Secured({"ROLE_SUPER_ADMIN", "ROLE_COMPANY_ADMIN"})
    public ResponseEntity getUserById(@PathVariable String userId) {
        return userService.getUserById(userId);
    }

    @PostMapping
    @Secured({"ROLE_SUPER_ADMIN", "ROLE_COMPANY_ADMIN"})
    public ResponseEntity saveUser(@Valid @RequestBody UserDto userDto, BindingResult result) {
        return userService.saveUser(userDto, result);
    }

    @PutMapping("/{userId}")
    @Secured({"ROLE_SUPER_ADMIN", "ROLE_COMPANY_ADMIN"})
    public ResponseEntity updateUser(@Valid @RequestBody UserDto userDto, BindingResult result, @PathVariable String userId) {
        return userService.updateUser(userDto, result, userId);
    }

    @DeleteMapping("/{userId}")
    @Secured({"ROLE_SUPER_ADMIN", "ROLE_COMPANY_ADMIN"})
    public ResponseEntity saveUser(@PathVariable String userId) {
        return userService.deleteUser(userId);
    }

}
