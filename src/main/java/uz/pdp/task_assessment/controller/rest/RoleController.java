package uz.pdp.task_assessment.controller.rest;

//Asatbek Xalimojnov 4/8/22 10:11 AM

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import uz.pdp.task_assessment.service.interfaces.RoleService;

import static uz.pdp.task_assessment.config.Constant.DEFAULT_PAGE;
import static uz.pdp.task_assessment.config.Constant.DEFAULT_PAGE_SIZE;

@RestController
@RequestMapping("/api/role")
@AllArgsConstructor
public class RoleController {

    private final RoleService roleService;

    @GetMapping
    @PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
    public ResponseEntity getAllRole(
            @RequestParam(required = false, defaultValue = DEFAULT_PAGE) int page,
            @RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE) int size
    ) {
        return roleService.getAdminRole(page, size);
    }

    @GetMapping("/company")
    @PreAuthorize("hasRole('ROLE_COMPANY_ADMIN')")
    public ResponseEntity getRoleCompany(
            @RequestParam(required = false, defaultValue = DEFAULT_PAGE) int page,
            @RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE) int size
    ) {
        return roleService.getCompanyRole(page, size);
    }
}

