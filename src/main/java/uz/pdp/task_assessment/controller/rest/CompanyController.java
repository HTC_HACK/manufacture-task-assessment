package uz.pdp.task_assessment.controller.rest;

import lombok.AllArgsConstructor;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.annotation.Secured;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.*;
import uz.pdp.task_assessment.dto.CompanyDto;
import uz.pdp.task_assessment.service.interfaces.CompanyService;

import javax.validation.Valid;

import static uz.pdp.task_assessment.config.Constant.DEFAULT_PAGE;
import static uz.pdp.task_assessment.config.Constant.DEFAULT_PAGE_SIZE;


//Asatbek Xalimojnov 4/7/22 9:15 PM


@RestController
@RequestMapping("/api/company")
@AllArgsConstructor
public class CompanyController {

    private final CompanyService companyService;

    @GetMapping
    @PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
    public ResponseEntity getAllCompanies(
            @RequestParam(required = false, defaultValue = DEFAULT_PAGE) int page,
            @RequestParam(required = false, defaultValue = DEFAULT_PAGE_SIZE) int size
    ) {
        return companyService.getAllCompanies(page, size);
    }

    @GetMapping("/{companyId}")
    @Secured({"ROLE_SUPER_ADMIN","ROLE_COMPANY_ADMIN"})
    public ResponseEntity getCompany(@PathVariable String companyId) {
        return companyService.findById(companyId);
    }

    @PostMapping
    @PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
    public ResponseEntity saveCompany(@Valid @RequestBody CompanyDto companyDto, BindingResult result) {
        return companyService.saveCompany(companyDto, result);
    }


    @PutMapping("/{companyId}")
    @Secured({"ROLE_SUPER_ADMIN","ROLE_COMPANY_ADMIN"})
    public ResponseEntity updateCompany(@Valid @RequestBody CompanyDto companyDto,
                                        BindingResult result,
                                        @PathVariable String companyId) {
        return companyService.updateCompany(companyDto, companyId,result);
    }


    @DeleteMapping("/{companyId}")
    @PreAuthorize("hasRole('ROLE_SUPER_ADMIN')")
    public ResponseEntity deleteCompany(@PathVariable String companyId) {
        return companyService.deleteCompany(companyId);
    }
}
