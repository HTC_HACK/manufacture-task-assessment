package uz.pdp.task_assessment.service.impl;

import lombok.AllArgsConstructor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import uz.pdp.task_assessment.dto.CompanyDto;
import uz.pdp.task_assessment.entity.company.Company;
import uz.pdp.task_assessment.entity.user.User;
import uz.pdp.task_assessment.payload.ApiResponse;
import uz.pdp.task_assessment.repo.CompanyRepo;
import uz.pdp.task_assessment.service.interfaces.CompanyService;
import uz.pdp.task_assessment.service.interfaces.UserService;
import uz.pdp.task_assessment.service.interfaces.ValidationService;

import java.util.List;


//Asatbek Xalimojnov 4/7/22 10:00 PM

@Service
@AllArgsConstructor
public class CompanyServiceImpl implements CompanyService {

    final CompanyRepo companyRepo;
    final UserService userService;
    final ValidationService validationService;

    @Override
    public ResponseEntity getAllCompanies(int page, int size) {

        User currentUser = userService.getCurrentUser();
        if (currentUser != null && userService.isSuperAdmin(currentUser.getRoles())) {
            Pageable pageable = PageRequest.of(page - 1, size);
            Page<Company> allCompanies = companyRepo.findAll(pageable);

            return new ResponseEntity<>(
                    new ApiResponse(true,
                            "success",
                            allCompanies
                    ), HttpStatus.OK);
        }

        return new ResponseEntity<>(
                new ApiResponse(false,
                        "access denied",
                        null
                ), HttpStatus.OK);

    }

    @Override
    public ResponseEntity findById(String companyId) {

        try {

            Company company = companyRepo.findById(companyId).get();
            User currentUser = userService.getCurrentUser();

            if (currentUser != null &&
                    ((
                            currentUser.getCompany().equals(company) &&
                                    userService.isCompanyAdmin(currentUser.getRoles()))
                            ||
                            userService.isSuperAdmin(currentUser.getRoles()))) {

                return new ResponseEntity<>(
                        new ApiResponse(true,
                                "success",
                                company
                        ), HttpStatus.OK);
            }

            return new ResponseEntity<>(
                    new ApiResponse(true,
                            "access denied",
                            null
                    ), HttpStatus.OK);

        } catch (Exception ignored) {
        }
        return new ResponseEntity<>(
                new ApiResponse(false,
                        "not found",
                        null
                ), HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity saveCompany(CompanyDto companyDto, BindingResult result) {

        if (result.hasErrors()) {

            return new ResponseEntity<>(
                    new ApiResponse(false,
                            "error",
                            validationService.errorMessage(result.getFieldErrors())
                    ), HttpStatus.BAD_REQUEST);

        }

        Company findByName = companyRepo.findByName(companyDto.getName());
        if (findByName != null) {
            return new ResponseEntity<>(
                    new ApiResponse(false,
                            "error",
                            "company already exist with this name"
                    ), HttpStatus.BAD_REQUEST);
        }

        Company company = new Company(
                companyDto.getName(),
                companyDto.getAddress(),
                companyDto.getEmail(),
                companyDto.getWebsite(),
                companyDto.getPhoneNumber()
        );

        try {
            companyRepo.save(company);
            return new ResponseEntity<>(
                    new ApiResponse(true,
                            "success",
                            company
                    ), HttpStatus.CREATED);

        } catch (Exception ignored) {
        }
        return new ResponseEntity<>(
                new ApiResponse(false,
                        "error",
                        null
                ), HttpStatus.BAD_REQUEST);
    }

    @Override
    public ResponseEntity updateCompany(CompanyDto companyDto, String companyId, BindingResult result) {

        if (companyRepo.findById(companyId).isPresent()) {

            Company findCompany = companyRepo.findById(companyId).get();


            User currentUser = userService.getCurrentUser();

            if (currentUser != null &&
                    (( currentUser.getCompany()!=null &&
                            currentUser.getCompany().equals(findCompany) &&
                                    userService.isCompanyAdmin(currentUser.getRoles()))
                            ||
                            userService.isSuperAdmin(currentUser.getRoles()))) {

                if (result.hasErrors()) {

                    return new ResponseEntity<>(
                            new ApiResponse(false,
                                    "error",
                                    validationService.errorMessage(result.getFieldErrors())
                            ), HttpStatus.BAD_REQUEST);

                }

                if (findCompany.getName().equals(companyDto.getName())) {
                    return new ResponseEntity<>(
                            new ApiResponse(false,
                                    "error",
                                    "company already exist with this name"
                            ), HttpStatus.BAD_REQUEST);
                }

                findCompany.setAddress(companyDto.getAddress());
                findCompany.setEmail(companyDto.getEmail());
                findCompany.setName(companyDto.getName());
                findCompany.setWebsite(companyDto.getWebsite());
                findCompany.setPhoneNumber(companyDto.getPhoneNumber());

                try {
                    companyRepo.save(findCompany);
                    return new ResponseEntity<>(
                            new ApiResponse(true,
                                    "updated",
                                    findCompany
                            ), HttpStatus.OK);

                } catch (Exception ignored) {
                }
                return new ResponseEntity<>(
                        new ApiResponse(false,
                                "error",
                                null
                        ), HttpStatus.BAD_REQUEST);

            }

            return new ResponseEntity<>(
                    new ApiResponse(false,
                            "access denied",
                            null
                    ), HttpStatus.OK);

        }

        return new ResponseEntity<>(
                new ApiResponse(false,
                        "error",
                        null
                ), HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity deleteCompany(String companyId) {

        try {

            companyRepo.deleteById(companyId);
            return new ResponseEntity<>(
                    new ApiResponse(true,
                            "success",
                            null
                    ), HttpStatus.OK);
        } catch (Exception ignored) {
        }
        return new ResponseEntity<>(
                new ApiResponse(false,
                        "not found",
                        companyId
                ), HttpStatus.NOT_FOUND);
    }


}
