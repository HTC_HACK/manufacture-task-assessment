package uz.pdp.task_assessment.service.impl;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.validation.BindingResult;
import uz.pdp.task_assessment.dto.UserDto;
import uz.pdp.task_assessment.entity.company.Company;
import uz.pdp.task_assessment.entity.company.Position;
import uz.pdp.task_assessment.entity.enums.RoleEnum;
import uz.pdp.task_assessment.entity.user.Role;
import uz.pdp.task_assessment.entity.user.User;
import uz.pdp.task_assessment.entity.user.UserDetail;
import uz.pdp.task_assessment.payload.ApiResponse;
import uz.pdp.task_assessment.repo.CompanyRepo;
import uz.pdp.task_assessment.repo.PositionRepo;
import uz.pdp.task_assessment.repo.RoleRepo;
import uz.pdp.task_assessment.repo.UserRepo;
import uz.pdp.task_assessment.service.interfaces.UserService;
import uz.pdp.task_assessment.service.interfaces.ValidationService;

import javax.validation.Validation;
import java.util.*;


//Asatbek Xalimojnov 4/7/22 10:56 PM

@Service
@RequiredArgsConstructor
@Transactional
@Slf4j
public class UserServiceImpl implements UserService, UserDetailsService {

    private final UserRepo userRepo;
    private final ValidationService validationService;
    private final RoleRepo roleRepo;
    private final PositionRepo positionRepo;
    private final CompanyRepo companyRepo;


    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {

        User user = userRepo.findByUsername(username);
        if (user == null) {
            log.error("User not found");
            throw new UsernameNotFoundException("User not found");
        } else {
            log.info("User found {} user ", username);
        }

        Collection<SimpleGrantedAuthority> authorities = new ArrayList<>();
        user.getRoles().forEach(role -> {
            authorities.add(new SimpleGrantedAuthority(role.getName()));
        });

        return new org.springframework.security.core.userdetails.User(user.getUsername(), user.getPassword(), authorities);

    }

    @Override
    public User getUser(String username) {
        log.info("Fetching user {}", username);
        return userRepo.findByUsername(username);
    }

    @Override
    public User getCurrentUser() {
        String username = (String) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        return userRepo.findByUsername(username);
    }

    @Override
    public boolean isSuperAdmin(Set<Role> roleSet) {
        for (Role role : roleSet) {
            if (role.getRoleEnum().equals(RoleEnum.SUPER_ADMIN)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public boolean isCompanyAdmin(Set<Role> roleSet) {
        for (Role role : roleSet) {
            if (role.getRoleEnum().equals(RoleEnum.COMPANY_ADMIN)) {
                return true;
            }
        }

        return false;
    }

    @Override
    public ResponseEntity getAllUsers(int page, int size) {
        User currentUser = getCurrentUser();

        if (currentUser != null && isSuperAdmin(currentUser.getRoles())) {
            Pageable pageable = PageRequest.of(page - 1, size);
            Page<User> allUsers = userRepo.findAll(pageable);

            return new ResponseEntity<>(
                    new ApiResponse(true,
                            "success",
                            allUsers
                    ), HttpStatus.OK);
        }

        if (currentUser != null && isCompanyAdmin(currentUser.getRoles())) {
            Pageable pageable = PageRequest.of(page - 1, size);
            Page<User> allUsers = userRepo.findUserCompany(pageable, currentUser.getCompany().getId());

            return new ResponseEntity<>(
                    new ApiResponse(true,
                            "success",
                            allUsers
                    ), HttpStatus.OK);
        }

        return new ResponseEntity<>(
                new ApiResponse(false,
                        "access denied",
                        null
                ), HttpStatus.OK);
    }

    @Override
    public ResponseEntity getUserById(String userId) {

        User currentUser = getCurrentUser();

        if (isSuperAdmin(currentUser.getRoles())) {

            if (userRepo.findById(userId).isPresent()) {
                return new ResponseEntity<>(
                        new ApiResponse(true,
                                "success",
                                userRepo.findById(userId).get()
                        ), HttpStatus.OK);
            }
            return new ResponseEntity<>(
                    new ApiResponse(false,
                            "not found",
                            null
                    ), HttpStatus.NOT_FOUND);
        } else if (isCompanyAdmin(currentUser.getRoles())) {
            if (userRepo.findById(userId).isPresent()) {
                User user = userRepo.findById(userId).get();
                if (user.getCompany().getId().equals(currentUser.getCompany().getId())) {
                    return new ResponseEntity<>(
                            new ApiResponse(true,
                                    "success",
                                    userRepo.findById(userId).get()
                            ), HttpStatus.OK);
                }
                return new ResponseEntity<>(
                        new ApiResponse(true,
                                "access denied",
                                null
                        ), HttpStatus.OK);
            }
            return new ResponseEntity<>(
                    new ApiResponse(false,
                            "not found",
                            null
                    ), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(
                new ApiResponse(false,
                        "error",
                        null
                ), HttpStatus.OK);
    }

    @Override
    public ResponseEntity saveUser(UserDto userDto, BindingResult result) {


        if (!isPassportSeria(userDto.getPassportSeria())) {
            return new ResponseEntity<>(
                    new ApiResponse(false,
                            "invalid passport seria",
                            null
                    ), HttpStatus.OK);
        }

        if (result.hasErrors()) {

            return new ResponseEntity<>(
                    new ApiResponse(false,
                            "error",
                            validationService.errorMessage(result.getFieldErrors())
                    ), HttpStatus.BAD_REQUEST);
        }

        User currentUser = getCurrentUser();

        Optional<Company> company = companyRepo.findById(userDto.getCompanyId());
        Optional<Position> position = positionRepo.findById(userDto.getPositionId());
        List<Role> roles = roleRepo.findAllById(userDto.getRoles());
        if (company.get() != null && position.get() != null && roles.size() > 0) {
            User user = new User();
            user.setUsername(userDto.getUsername());
            user.setPhoneNumber(userDto.getPhoneNumber());
            user.setPassword(getEncoder().encode(userDto.getPassword()));
            user.setPassportSeria(userDto.getPassportSeria());
            user.setAddress(userDto.getAddress());
            user.setFullName(new UserDetail(userDto.getFirstName(), userDto.getLastName(), userDto.getFatherName()));
            user.setCompany(company.get());
            user.setPosition(position.get());
            Set<Role> roleSet = new HashSet<>();
            for (Role role : roles) {
                roleSet.add(role);
            }
            user.setRoles(roleSet);

            boolean isSaved = false;
            if (isSuperAdmin(currentUser.getRoles())) {

                try {
                    userRepo.save(user);
                    isSaved = true;
                } catch (Exception ignored) {
                }
            } else if (isCompanyAdmin(currentUser.getRoles())) {
                try {
                    userRepo.save(user);
                    isSaved = true;
                } catch (Exception ignored) {
                }
            }
            return new ResponseEntity<>(
                    new ApiResponse(true,
                            isSaved ? "success" : "error",
                            userDto
                    ), HttpStatus.OK);
        }

        return new ResponseEntity<>(
                new ApiResponse(true,
                        "error",
                        null
                ), HttpStatus.OK);
    }

    @Override
    public ResponseEntity updateUser(UserDto userDto, BindingResult result, String userId) {

        if (userRepo.findById(userId).isPresent()) {
            User findUser = userRepo.findById(userId).get();
            if (!isPassportSeria(userDto.getPassportSeria())) {
                return new ResponseEntity<>(
                        new ApiResponse(false,
                                "invalid passport seria",
                                null
                        ), HttpStatus.OK);
            }

            if (result.hasErrors()) {

                return new ResponseEntity<>(
                        new ApiResponse(false,
                                "error",
                                validationService.errorMessage(result.getFieldErrors())
                        ), HttpStatus.BAD_REQUEST);
            }

            User currentUser = getCurrentUser();

            Optional<Company> company = companyRepo.findById(userDto.getCompanyId());
            Optional<Position> position = positionRepo.findById(userDto.getPositionId());
            List<Role> roles = roleRepo.findAllById(userDto.getRoles());
            if (company.get() != null && position.get() != null && roles.size() > 0) {

                findUser.setUsername(userDto.getUsername());
                findUser.setPhoneNumber(userDto.getPhoneNumber());
                findUser.setPassword(getEncoder().encode(userDto.getPassword()));
                findUser.setPassportSeria(userDto.getPassportSeria());
                findUser.setAddress(userDto.getAddress());
                findUser.setFullName(new UserDetail(userDto.getFirstName(), userDto.getLastName(), userDto.getFatherName()));
                findUser.setCompany(company.get());
                findUser.setPosition(position.get());
                Set<Role> roleSet = new HashSet<>();
                for (Role role : roles) {
                    roleSet.add(role);
                }
                findUser.setRoles(roleSet);

                boolean isSaved = false;
                boolean access = false;
                if (isSuperAdmin(currentUser.getRoles())) {

                    try {
                        userRepo.save(findUser);
                        isSaved = true;
                        access=true;
                    } catch (Exception ignored) {
                    }
                } else if (isCompanyAdmin(currentUser.getRoles()) && findUser.getCompany().equals(currentUser.getCompany())) {
                    try {
                        userRepo.save(findUser);
                        isSaved = true;
                        access=true;
                    } catch (Exception ignored) {
                    }
                }
                if (access) {
                    return new ResponseEntity<>(
                            new ApiResponse(isSaved ? true : false,
                                    isSaved ? "success" : "error",
                                    userDto
                            ), HttpStatus.OK);
                }
                return new ResponseEntity<>(
                        new ApiResponse(false,
                                "access denied",
                                null
                        ), HttpStatus.OK);
            }

            return new ResponseEntity<>(
                    new ApiResponse(true,
                            "error",
                            null
                    ), HttpStatus.OK);
        }

        return new ResponseEntity<>(
                new ApiResponse(true,
                        "not found",
                        null
                ), HttpStatus.NOT_FOUND);
    }

    @Override
    public ResponseEntity deleteUser(String userId) {
        User currentUser = getCurrentUser();

        if (isSuperAdmin(currentUser.getRoles())) {

            if (userRepo.findById(userId).isPresent()) {
                try {
                    userRepo.deleteById(userId);
                    return new ResponseEntity<>(
                            new ApiResponse(true,
                                    "deleted",
                                    userId
                            ), HttpStatus.OK);
                } catch (Exception ignored) {
                }
                return new ResponseEntity<>(
                        new ApiResponse(true,
                                "error deleting",
                                userId
                        ), HttpStatus.OK);
            }
            return new ResponseEntity<>(
                    new ApiResponse(false,
                            "not found",
                            null
                    ), HttpStatus.NOT_FOUND);

        } else if (isCompanyAdmin(currentUser.getRoles())) {
            if (userRepo.findById(userId).isPresent()) {
                User user = userRepo.findById(userId).get();
                if (user.getCompany().getId().equals(currentUser.getCompany().getId())) {

                    try {
                        userRepo.deleteById(userId);
                        return new ResponseEntity<>(
                                new ApiResponse(true,
                                        "deleted",
                                        userId
                                ), HttpStatus.OK);
                    } catch (Exception ignored) {
                    }
                    return new ResponseEntity<>(
                            new ApiResponse(true,
                                    "error deleting",
                                    userId
                            ), HttpStatus.OK);
                }
                return new ResponseEntity<>(
                        new ApiResponse(true,
                                "access denied",
                                null
                        ), HttpStatus.OK);
            }
            return new ResponseEntity<>(
                    new ApiResponse(false,
                            "not found",
                            null
                    ), HttpStatus.NOT_FOUND);
        }
        return new ResponseEntity<>(
                new ApiResponse(false,
                        "error",
                        null
                ), HttpStatus.OK);
    }

    public boolean isPassportSeria(String passport) {

        if (passport.length() == 9) {
            if (Character.isLetter(passport.charAt(0)) && Character.isLetter(passport.charAt(1))) {

                try {
                    String number = passport.substring(2, 9);
                    int num = Integer.parseInt(number);
                    System.out.println(num);
                    return true;
                } catch (Exception ignored) {
                    return false;
                }
            }
            return false;
        }

        return false;
    }

    public PasswordEncoder getEncoder() {
        return new BCryptPasswordEncoder();
    }


}
