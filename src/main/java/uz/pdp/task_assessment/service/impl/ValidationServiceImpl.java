package uz.pdp.task_assessment.service.impl;

import org.springframework.stereotype.Service;
import org.springframework.validation.FieldError;
import uz.pdp.task_assessment.service.interfaces.ValidationService;

import java.util.ArrayList;
import java.util.List;


//Asatbek Xalimojnov 4/8/22 8:29 AM
@Service
public class ValidationServiceImpl implements ValidationService {

    @Override
    public List<String> errorMessage(List<FieldError> fieldErrors) {
        List<String> errors = new ArrayList<>();
        for (FieldError fieldError : fieldErrors) {
            errors.add(fieldError.getDefaultMessage());
        }

        return errors;
    }
}
