package uz.pdp.task_assessment.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.task_assessment.entity.company.Company;
import uz.pdp.task_assessment.entity.company.Position;
import uz.pdp.task_assessment.payload.ApiResponse;
import uz.pdp.task_assessment.repo.PositionRepo;
import uz.pdp.task_assessment.service.interfaces.PositionService;


//Asatbek Xalimojnov 4/8/22 10:05 AM

@Service
public class PositionServiceImpl implements PositionService {

    @Autowired
    private PositionRepo positionRepo;

    @Override
    public ResponseEntity getAllPositions(int page, int size) {
        Pageable pageable = PageRequest.of(page - 1, size);
        Page<Position> allPosition = positionRepo.findAll(pageable);

        return new ResponseEntity<>(
                new ApiResponse(true,
                        "success",
                        allPosition
                ), HttpStatus.OK);
    }
}
