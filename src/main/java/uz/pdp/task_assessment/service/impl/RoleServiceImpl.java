package uz.pdp.task_assessment.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import uz.pdp.task_assessment.entity.user.Role;
import uz.pdp.task_assessment.payload.ApiResponse;
import uz.pdp.task_assessment.repo.RoleRepo;
import uz.pdp.task_assessment.service.interfaces.RoleService;


//Asatbek Xalimojnov 4/8/22 10:12 AM


@Service
public class RoleServiceImpl implements RoleService {

    @Autowired
    private RoleRepo roleRepo;

    @Override
    public ResponseEntity getAdminRole(int page, int size) {
        Pageable pageable = PageRequest.of(page - 1, size);
        Page<Role> allRoles = roleRepo.findAll(pageable);

        return new ResponseEntity<>(
                new ApiResponse(true,
                        "success",
                        allRoles
                ), HttpStatus.OK);
    }

    @Override
    public ResponseEntity getCompanyRole(int page, int size) {
        Pageable pageable = PageRequest.of(page - 1, size);
        Page<Role> allRoles = roleRepo.getCompanyRole(pageable);

        return new ResponseEntity<>(
                new ApiResponse(true,
                        "success",
                        allRoles
                ), HttpStatus.OK);
    }
}
