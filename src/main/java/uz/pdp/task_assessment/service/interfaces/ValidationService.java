package uz.pdp.task_assessment.service.interfaces;

import org.springframework.validation.FieldError;

import java.util.List;

public interface ValidationService {
    List<String> errorMessage(List<FieldError> fieldError);
}
