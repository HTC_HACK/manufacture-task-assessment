package uz.pdp.task_assessment.service.interfaces;

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import uz.pdp.task_assessment.dto.CompanyDto;

public interface CompanyService {
    ResponseEntity getAllCompanies(int page,int size);

    ResponseEntity findById(String companyId);

    ResponseEntity saveCompany(CompanyDto companyDto, BindingResult result);

    ResponseEntity updateCompany(CompanyDto companyDto, String companyId,BindingResult result);

    ResponseEntity deleteCompany(String companyId);
}
