package uz.pdp.task_assessment.service.interfaces;

import org.springframework.http.ResponseEntity;

public interface PositionService {
    ResponseEntity getAllPositions(int page, int size);
}
