package uz.pdp.task_assessment.service.interfaces;

import org.springframework.http.ResponseEntity;

public interface RoleService {

    ResponseEntity getAdminRole(int page, int size);
    ResponseEntity getCompanyRole(int page, int size);
}
