package uz.pdp.task_assessment.service.interfaces;


//Asatbek Xalimojnov 4/7/22 10:55 PM

import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import uz.pdp.task_assessment.dto.UserDto;
import uz.pdp.task_assessment.entity.user.Role;
import uz.pdp.task_assessment.entity.user.User;

import java.util.Set;

public interface UserService {

    User getUser(String username);

    User getCurrentUser();

    boolean isSuperAdmin(Set<Role> roleSet);

    boolean isCompanyAdmin(Set<Role> roleSet);

    ResponseEntity getAllUsers(int page, int size);

    ResponseEntity getUserById(String userId);

    ResponseEntity saveUser(UserDto userDto, BindingResult result);

    ResponseEntity updateUser(UserDto userDto, BindingResult result, String userId);

    ResponseEntity deleteUser(String userId);

}
