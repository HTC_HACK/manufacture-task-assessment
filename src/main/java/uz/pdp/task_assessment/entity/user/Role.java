package uz.pdp.task_assessment.entity.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.task_assessment.entity.absEntity.AbsEntity;
import uz.pdp.task_assessment.entity.enums.RoleEnum;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data

//Asatbek Xalimojnov 4/7/22 9:01 PM

@Entity(name = "roles")
public class Role extends AbsEntity {

    private String name;
    @Enumerated(EnumType.STRING)
    private RoleEnum roleEnum;
}
