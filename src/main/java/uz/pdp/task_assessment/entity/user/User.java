package uz.pdp.task_assessment.entity.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.task_assessment.entity.absEntity.AbsEntity;
import uz.pdp.task_assessment.entity.company.Company;
import uz.pdp.task_assessment.entity.company.Position;

import javax.persistence.*;
import java.util.HashSet;
import java.util.Set;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data

//Asatbek Xalimojnov 4/7/22 8:55 PM

@Entity(name = "users")
public class User extends AbsEntity {


    @Column(unique = true)
    private String username;

    @Column(unique = true)
    private String phoneNumber;

    private String password;

    @Column(unique = true)
    private String passportSeria;

    private String address;

    @Embedded
    private UserDetail fullName;

    @ManyToOne(fetch = FetchType.EAGER)
    private Company company;

    @ManyToOne(fetch = FetchType.EAGER)
    private Position position;

    @ManyToMany(fetch = FetchType.EAGER)
    @JoinTable(name = "user_roles",
            joinColumns = @JoinColumn(name = "user_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "role_id", referencedColumnName = "id"))
    private Set<Role> roles = new HashSet<>();



}
