package uz.pdp.task_assessment.entity.user;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Embeddable;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asatbek Xalimojnov 4/7/22 8:56 PM

@Embeddable
public class UserDetail {

    private String firstName;
    private String lastName;
    private String fatherName;

}
