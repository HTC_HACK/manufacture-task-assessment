package uz.pdp.task_assessment.entity.company;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.task_assessment.entity.absEntity.AbsEntity;

import javax.persistence.Entity;
import javax.persistence.ManyToOne;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data

//Asatbek Xalimojnov 4/7/22 8:59 PM

@Entity(name = "positions")
public class Position extends AbsEntity {
    private String name;

}
