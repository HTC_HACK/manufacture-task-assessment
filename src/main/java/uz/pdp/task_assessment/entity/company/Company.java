package uz.pdp.task_assessment.entity.company;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
import uz.pdp.task_assessment.entity.absEntity.AbsEntity;

import javax.persistence.Entity;

@EqualsAndHashCode(callSuper = true)
@AllArgsConstructor
@NoArgsConstructor
@Data

//Asatbek Xalimojnov 4/7/22 8:53 PM

@Entity(name = "companies")
public class Company extends AbsEntity {

    private String name;
    private String address;
    private String email;
    private String website;
    private String phoneNumber;

}
