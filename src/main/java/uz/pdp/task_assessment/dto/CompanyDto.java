package uz.pdp.task_assessment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotBlank;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asatbek Xalimojnov 4/8/22 8:15 AM

public class CompanyDto {

    @NotBlank(message = "Name Required")
    private String name;
    @NotBlank(message = "Address Required")
    private String address;
    @NotBlank(message = "Email Required")
    private String email;
    @NotBlank(message = "Website Required")
    private String website;
    @NotBlank(message = "PhoneNumber Required")
    private String phoneNumber;
}
