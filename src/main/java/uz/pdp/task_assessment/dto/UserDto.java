package uz.pdp.task_assessment.dto;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import javax.validation.constraints.NotBlank;
import java.util.List;
import java.util.Set;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asatbek Xalimojnov 4/8/22 9:30 AM

public class UserDto {

    @NotBlank(message = "Username Required")
    private String username;
    @NotBlank(message = "PhoneNumber Required")
    private String phoneNumber;
    @NotBlank(message = "Password Required")
    private String password;

    @NotBlank(message = "PassportSeria Required")
    private String passportSeria;

    @NotBlank(message = "Address Required")
    private String address;

    @NotBlank(message = "FirstName Required")
    private String firstName;
    @NotBlank(message = "LastName Required")
    private String lastName;
    @NotBlank(message = "FatherName Required")
    private String fatherName;
    @NotBlank(message = "CompanyId Required")
    private String companyId;
    @NotBlank(message = "PositionId Required")
    private String positionId;

    private Set<String> roles;

}
