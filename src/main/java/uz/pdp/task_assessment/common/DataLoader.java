package uz.pdp.task_assessment.common;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.CommandLineRunner;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Component;
import uz.pdp.task_assessment.entity.company.Company;
import uz.pdp.task_assessment.entity.company.Position;
import uz.pdp.task_assessment.entity.enums.RoleEnum;
import uz.pdp.task_assessment.entity.user.Role;
import uz.pdp.task_assessment.entity.user.User;
import uz.pdp.task_assessment.entity.user.UserDetail;
import uz.pdp.task_assessment.repo.CompanyRepo;
import uz.pdp.task_assessment.repo.PositionRepo;
import uz.pdp.task_assessment.repo.RoleRepo;
import uz.pdp.task_assessment.repo.UserRepo;

import java.util.HashSet;
import java.util.Set;

//Asatbek Xalimojnov 4/7/22 9:16 PM

@Component
public class DataLoader implements CommandLineRunner {

    @Value("${spring.sql.init.mode}")
    String initMode;

    final CompanyRepo companyRepo;
    final PositionRepo positionRepo;
    final UserRepo userRepo;
    final RoleRepo roleRepo;
    final PasswordEncoder passwordEncoder;

    public DataLoader(CompanyRepo companyRepo, PositionRepo positionRepo, UserRepo userRepo, RoleRepo roleRepo, PasswordEncoder passwordEncoder) {
        this.companyRepo = companyRepo;
        this.positionRepo = positionRepo;
        this.userRepo = userRepo;
        this.roleRepo = roleRepo;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public void run(String... args) throws Exception {

        if (initMode.equals("always")) {


            //company
            Company company1 = new Company("GMUzbekistan", "Asaka", "gmuzauto@gm.com", "uzauto.uz", "+998770807766");
            Company company2 = new Company("Beeline", "Tashkent", "beeline@gm.com", "beeline.uz", "+998775605566");
            Company company3 = new Company("Ucell", "Tashkent", "ucell@gm.com", "ucell.uz", "+998770807744");
            Company company4 = new Company("UzMobile", "Tashkent", "uzmobile@gm.com", "uzmobile.uz", "+998770807733");
            Company company5 = new Company("Humans", "Tashkent", "humans@gm.com", "humans.uz", "+998770807711");

            Company savedCompany1 = companyRepo.save(company1);
            Company savedCompany2 = companyRepo.save(company2);
            Company savedCompany3 = companyRepo.save(company3);
            Company savedCompany4 = companyRepo.save(company4);
            Company savedCompany5 = companyRepo.save(company5);


            //position
            Position position1 = new Position("Director");
            Position position2 = new Position("Manager");
            Position position3 = new Position("Employee");

            Position savedPosition1 = positionRepo.save(position1);
            Position savedPosition2 = positionRepo.save(position2);
            Position savedPosition3 = positionRepo.save(position3);


            //role
            Role super_admin = new Role("ROLE_SUPER_ADMIN", RoleEnum.SUPER_ADMIN);
            Role company_admin = new Role("ROLE_COMPANY_ADMIN", RoleEnum.COMPANY_ADMIN);
            Role other = new Role("ROLE_OTHER", RoleEnum.OTHER);

            Role savedSuperAdmin = roleRepo.save(super_admin);
            Role savedCompanyAdmin = roleRepo.save(company_admin);
            Role savedOther = roleRepo.save(other);

            Set<Role> super_admin_role = new HashSet<>();
            super_admin_role.add(savedSuperAdmin);

            Set<Role> company_admin_role = new HashSet<>();
            company_admin_role.add(savedCompanyAdmin);
            company_admin_role.add(savedOther);

            Set<Role> company_employee_role = new HashSet<>();
            company_employee_role.add(savedOther);


            //user
            User user_super_admin = new User(
                    "asadbek",
                    "+998994436224",
                    passwordEncoder.encode("1111"),
                    "AB5447316",
                    "Asaka",
                    new UserDetail("Asadbek", "Xalimjonov", "Soyibjon"),
                    null,
                    null,
                    super_admin_role
            );

            userRepo.save(user_super_admin);


            User company_admin1 = new User(
                    "ali",
                    "+998991005060",
                    passwordEncoder.encode("1111"),
                    "AB5447896",
                    "Tashkent",
                    new UserDetail("Ali", "Jalolov", "Nuriddin"),
                    savedCompany1,
                    savedPosition1,
                    company_admin_role
            );

            userRepo.save(company_admin1);

            User company_admin2 = new User(
                    "eldor",
                    "+998991005066",
                    passwordEncoder.encode("1111"),
                    "AB5447886",
                    "Tashkent",
                    new UserDetail("Eldor", "Choriyev", "Asad"),
                    savedCompany2,
                    savedPosition1,
                    company_admin_role
            );

            userRepo.save(company_admin2);

            User company_employee1 = new User(
                    "nurbek",
                    "+998991008066",
                    passwordEncoder.encode("1111"),
                    "AB5448886",
                    "Tashkent",
                    new UserDetail("Nurbek", "Ubaydullayev", "Islom"),
                    savedCompany1,
                    savedPosition2,
                    company_employee_role
            );

            userRepo.save(company_employee1);

            User company_employee2 = new User(
                    "saidbek",
                    "+998931005066",
                    passwordEncoder.encode("1111"),
                    "AF8447886",
                    "Tashkent",
                    new UserDetail("Saidbek", "Xalimjonov", "Ulugbek"),
                    savedCompany2,
                    savedPosition2,
                    company_employee_role
            );

            userRepo.save(company_employee2);


        }

    }
}
