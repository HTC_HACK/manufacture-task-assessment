package uz.pdp.task_assessment.payload;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@AllArgsConstructor
@NoArgsConstructor
@Data

//Asatbek Xalimojnov 4/7/22 9:55 PM

public class ApiResponse {

    private boolean status;
    private String message;
    private Object data;

}