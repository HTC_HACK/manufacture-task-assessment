package uz.pdp.task_assessment;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class TaskAssessmentApplication {

    public static void main(String[] args) {
        SpringApplication.run(TaskAssessmentApplication.class, args);
    }

}
